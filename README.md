# Readme source.smk, st2_preparer.py, source_pie.R

---
## Functionality

This pipeline takes a sample and potential sources consisting of tab separated 
taxonomical units as input. It then calculates the contamination percentages
of the potential sources in the given sample.  
Finally, it presents a pie chart in which this contamination can be seen.


---
## File formats

All files, input and output are tab separated.

### sample.txt and source.txt files
    count; domain; phylum; class; order; family; genus; species
    etc.

### otus.txt
    id;     sample1;    sample2;    etc.
    otu1;
    otu2;
    etc.

### metadata.txt (pipeline sample is usually the only sink, the rest will be sources)
    id;         SourceSink;
    sample1;
    sample2;
    etc.
    
### lineages.txt
    id;     Lineage/Taxonomy;
    otu1;
    otu2;
    etc.

### mixing_proportions.txt
All samples and sources and their corresponding percentages.  
Unknown means a contamination source was not found, indicating either an unknown source or a samples unique identity.

    SampleID source1 source2 source3 Unknown
    Sample1 %   %   %   %

---
## Running the file
After making sure you are in this project's working directory, one can simply type the 
following command in the terminal:  

    snakemake --snakefile sourcetracking/source.smk --cores 16 --use-conda -j;

Or the following command if you wish not to use conda:

    snakemake --snakefile sourcetracking/source.smk --cores 16;

---
## Scripts

### st2_preparer.py

Takes the input samples and sources and converts those into a BIOM OTU format and its corresponding mapping file. Both 
of which are then taken as input by the sourcetracker2.

### source_pie.R

Takes the mixing_proportions.txt file as input and generates a contamination pie chart.

### multi_pie.py

Currently unused script that can be used for the creating of multiple pie charts when multiple samples are given at once.

---

## Execute prerequisites

* Must have conda version 4.10 or higher
* Must have Python 3.8 or higher
* Must have R 4.1 or higher
* Must have sourcetracker2 plugin installed

---

## Contact

**Carlo van Buiten**  

For any problems or questions:  
**c.van.buiten@st.hanze.nl** or **carlobuiten@hotmail.com**


