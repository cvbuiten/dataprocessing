configfile: "dataprocessing/sourcetracker/config.yaml"
cores_usage = config["cores"]

rule all:
    input:
        #"source_tracker/source.svg",
        config["workdir"] + "/output/otus.txt",
        config["workdir"] + "/output/mapping.txt",
        config["workdir"] + "/output/lineages.txt",

        config["workdir"] + "/output/source/mixing_proportions.txt",
        config["workdir"] + "/output/source/source.jpg"
    message:
        "To execute"

rule prepare:
    output:
        otus = config["workdir"] + "/output/otus.txt",
        mapping = config["workdir"] + "/output/mapping.txt",
        lineages = config["workdir"] + "/output/lineages.txt"
    shell:
        "python source_tracker/st2_preparer.py -i {config[workdir]}/sample.txt -o {config[workdir]}/output -s {config[sourcedir]}"

rule source:
    input:
        rules.prepare.output
    output:
        config["workdir"] + "/output/source/mixing_proportions.txt"
    conda:
        "../yaml/st2.yaml"
    shell:
        "rm -rf {config[workdir]}/output/source;"
        "sourcetracker2 gibbs -i {config[workdir]}/output/otus.txt -m {config[workdir]}/output/mapping.txt -o {config[workdir]}/output/source"

rule visualize:
    input:
        rules.source.output
    output:
        config["workdir"] + "/output/source/source.jpg"
    shell:
        "Rscript source_tracker/source_pie.R {config[workdir]}/output/source/mixing_proportions.txt {config[workdir]}/output/source/source.jpg"

rule dag:
    output:
        "source_tracker/source.svg"
    shell:
        "snakemake --snakefile source_tracker/source.smk --forceall --dag | dot -Tsvg > source_tracker/source.svg"