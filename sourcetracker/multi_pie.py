import sys
import matplotlib.pyplot as plt
import argparse


class PieChart:
    def __init__(self):
        args = argparser()
        self.inputfile = args.inputfile
        self.outputfile = args.outputfile
        self.labels = []
        self.slices = []
        self.samples = []

    def open(self):
        with open(self.inputfile) as file:
            for line in file:
                if line.startswith("SampleID"):
                    self.labels = line.split("\t")[1:]
                else:
                    line = line.split("\t")
                    self.samples.append(line[1])
                    self.slices.append(line[1:])

    def chart(self):
        i = 0
        for sample in self.samples:
            print("Pie Chart for Sample: {}".format(sample))
            plt.pie(self.slices[i], labels=self.labels)
            plt.show()
            i += 1


def argparser():
    """
    Reads the arguments from the command line.
    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputfile",
                        help="Input file must be a mixing proportions file.")
    parser.add_argument("-o", "--outputfile",
                        help="Desired path for the output pie chart.")

    args = parser.parse_args()
    return args


def main():
    instance = PieChart()
    instance.open()
    instance.chart()


if __name__ == '__main__':
    sys.exit(main())
