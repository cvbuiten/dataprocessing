import argparse
import sys
import os


class BiomSub:
    """
    Formats taxon tables into a text format comparable to BIOM.
    """
    def __init__(self):
        """
        Takes a krona.txt input file from the main MinION pipeline.
        """
        args = argparser()
        self.inputfile = args.inputfile
        self.outputdir = args.outputdir
        self.sourcedir = args.sourcedir
        self.id_tracker = 0
        self.entries = []
        self.header = "#OTU ID\tSample1"
        self.lineages = []

    def create(self):
        """
        Converts taxonomy table of sample 1 into a source-trackable format for entry into an OTU table.
        """
        with open(self.inputfile) as table:
            for otu in table:
                self.id_tracker += 1
                line = otu.strip().split("\t", 1)
                count = line[0]
                lineage = line[1].replace("\t", ";")

                self.lineages.append(lineage)

                entry = str(self.id_tracker) + "\t" + count
                self.entries.append(entry)

    def add(self, source):
        """
        Adds additional samples or sources to the OTU table.
        """
        sample_lineages = []
        with open(source) as table:
            for otu in table:
                line = otu.strip().split("\t", 1)
                count = line[0]
                lineage = line[1].replace("\t", ";")
                sample_lineages.append(lineage)  # Keep track of the sample's lineages.

                if lineage in self.lineages:
                    self.entries[self.lineages.index(lineage)] += "\t{}".format(count)  # Add OTU count for new sample.
                else:
                    counters = ""
                    self.id_tracker += 1
                    for i in self.header.split("\t"):  # Add 0 counts to the new OTU for all existing samples.
                        if i != "#OTU ID":
                            counters += "\t0"
                    counters += "\t{}".format(count)

                    entry = str(self.id_tracker) + counters  # Add the new OTU.

                    self.entries.append(entry)
                    self.lineages.append(lineage)

        self.header += "\t{}".format(source.split(".")[0].split("/")[-1])

        for lineage in self.lineages:
            if lineage not in sample_lineages:
                self.entries[self.lineages.index(lineage)] += "\t0"

    def sources(self):
        """
        Calls on the add method for every source in the source folder and finishes by writing the output.
        """
        for source in os.listdir(self.sourcedir):
            self.add(self.sourcedir + "/" + source)

        self.write_table(self.outputdir)

    def write_metadata(self):
        """
        Writes mapping data to mapping.txt in the output directory.
        """
        with open(self.outputdir + "/mapping.txt", "a") as file:
            file.write("#SampleID\tEnv\tSourceSink\n")
            for sample in self.header.split("\t")[1:]:
                if sample == "Sample1":
                    file.write("Sample1\tmonster\tsink\n")
                else:
                    file.write(sample + "\t{}\tsource\n".format(sample))

    def write_lineages(self):
        """
        Writes lineage data to lineages.txt in the output directory.
        """
        with open(self.outputdir + "/lineages.txt", "a") as file:
            file.write("#OTU ID\tConsensus Lineage\n")
            i = 1
            for lineage in self.lineages:
                file.write(str(i) + "\t" + lineage + "\n")
                i += 1

    def write_table(self, dir_name=""):
        """
        Writes OTU table to otus.txt in the output directory.
        """
        if not os.path.isdir(dir_name):
            os.mkdir(dir_name)
        with open(dir_name + "/otus.txt", "a") as file:
            file.write(self.header + "\n")
            for entry in self.entries:
                file.write(entry + "\n")


def argparser():
    """
    Reads the arguments from the command line.
    :return: command line arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--inputfile",
                        help="Input file must be a taxon table file.")
    parser.add_argument("-o", "--outputdir",
                        help="Desired path for the output directory.")
    parser.add_argument("-s", "--sourcedir",
                        help="Directory in which the sources to consider are found.")

    args = parser.parse_args()
    return args


def main():
    instance = BiomSub()
    instance.create()
    instance.sources()
    # instance.write_table()
    instance.write_lineages()
    instance.write_metadata()


if __name__ == '__main__':
    sys.exit(main())
